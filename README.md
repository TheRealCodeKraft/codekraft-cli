Cli scripts to help developers life at codekraft
================================================

Dependencies
------------

As Linux lovers, we only need a terminal to install and interact with our
development environment at CodeKraft. It can be `bash`, `zsh`, `iTerm2`… For
instance, all CodeKraft cli scripts are developed in bash (they use or should
use `#!/usr/bin/env bash` shebang).

Other dependencies are:

* `docker`
* `docker-compose`
* `git`
* `curl` (or other web calls command)

`ck-init`
---------

`ck-init` script install everything developer needs to work on codekraft
project. It is not even necessary to clone this project, you just have to
download and run it with following commands in the path you want to deploy
CodeKraft environment:

```bash
curl https://gitlab.com/TheRealCodeKraft/codekraft-cli/raw/master/ck-bigbang | bash -s
```

Use `-- -p` at the end of previous command to install codekraft-projects private
repository. Only for codekraft members.

### What `ck-init` do?

Actually, `ck-init` execute following steps:

* Create shared resource folders in `.codekraft`
* Clone base repos
* Build Docker images
  To avoid some files right drawbacks in development environment (see
  https://boxboat.com/2017/07/25/fixuid-change-docker-container-uid-gid/)
