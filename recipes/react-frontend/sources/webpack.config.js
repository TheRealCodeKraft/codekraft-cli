const webpack = require('webpack');
const path = require('path');

const paths = require('./webpack/config').paths;
const outputFiles = require('./webpack/config').outputFiles;
const rules = require('./webpack/config').rules;
const plugins = require('./webpack/config').plugins;
const resolve = require('./webpack/config').resolve;
const IS_PRODUCTION = require('./webpack/config').IS_PRODUCTION;
const IS_DEVELOPMENT = require('./webpack/config').IS_DEVELOPMENT;
const PUBLIC_PATH = require('./webpack/config').PUBLIC_PATH;

const devServer = require('./webpack/dev-server').devServer;
devServer.port = process.env.PORT || devServer.port || 3002

const DashboardPlugin = require('webpack-dashboard/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');


// Default client app entry file
const entry = [
  path.join(paths.javascript, 'client.js'),
];

plugins.push(
  // Builds index.html from template
  new HtmlWebpackPlugin({
    template: path.join(paths.source, 'index.html'),
    path: paths.build,
    filename: 'index.html',
    minify: {
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true,
      removeComments: true,
      useShortDoctype: true,
    },
  })
);

if (IS_DEVELOPMENT) {
  // Development plugins
  plugins.push(
    // Enables HMR
    new webpack.HotModuleReplacementPlugin(),
    // Don't emmit build when there was an error while compiling
    // No assets are emitted that include errors
    new webpack.NoEmitOnErrorsPlugin(),
    // Webpack dashboard plugin
    new DashboardPlugin()
  );

  // In development we add 'react-hot-loader' for .js/.jsx files
  // Check rules in config.js
  //rules[0].use.unshift('react-hot-loader/webpack');
  //entry.unshift('react-hot-loader/patch');
}

// Webpack config
module.exports = {
  devtool: IS_PRODUCTION ? false : 'source-map',
  context: paths.javascript,
  mode: IS_PRODUCTION ? 'production': 'development',
  entry,
  output: {
    path: paths.build,
    publicPath: PUBLIC_PATH,
    filename: outputFiles.client,
  },
  module: {
    rules,
  },
  //target: 'node',
  node: {fs: "empty"},
  resolve,
  plugins,
  devServer,
  optimization: {
    splitChunks: {},
    minimize: IS_PRODUCTION
  }
};
