Installation
============

Pour installer le projet, il faut utiliser la commande suivante.

```
curl -s 'https://gitlab.com/TheRealCodeKraft/codekraft-cli/raw/master/ck-init-frontend-only' \
  | bash -s [[PROJECT]]-frontend /chemin/vers/installation/du/projet
```

À l'issue de la procédure d'installation, 2 containers docker devrait être
lancés. Pour vérifier cela, vous pouvez lancer la commande suivante **dans le
répertoire du projet** :

```
docker-compose -f test/docker-compose.yml ps
```

La sortie devrait ressembler à cela :

```
     Name                   Command             State           Ports         
------------------------------------------------------------------------------
[[PROJECT]]_api_1        fixuid -q /app/command-test   Up      0.0.0.0:3000->3000/tcp
[[PROJECT]]_frontend_1   fixuid -q /app/command        Up      0.0.0.0:3002->3002/tcp
```

Vous pouvez aussi tester le frontend à l'adresse http://localhost:3002.
L'application React devrait être lancée.
