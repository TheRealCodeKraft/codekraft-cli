import { Bootstrap } from 'codekraft-react-frontend'
import '../scss/app.scss';

import 'babel-polyfill';

const config = {
  basename: process.env.PUBLIC_PATH,
  clients: [
  ],
  navigation: {
  }
}

Bootstrap.launch(config)
