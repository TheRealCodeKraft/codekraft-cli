const http = require('http')
const fs = require('fs')
const path = require('path')

const hostname = process.env.HOST.replace(/ /g, '')
const port = process.env.PORT

const server = http.createServer((request, response) => {
  response.statusCode = 200
  response.setHeader('Content-Type', 'application/json')
  response.setHeader('Access-Control-Allow-Origin', '*')

  if (request.method === 'OPTIONS') {
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
    response.setHeader('Access-Control-Allow-Headers', 'authorization,content-type')
    response.end('')
    return
  }

  const [ resource, query ] = urlToFile(request.url)
  const resourcePath = path.join(
    'test', 'resources',
    resource,
    request.method.toLowerCase(),
    (query || 'empty') + '.json'
  )

  fs.readFile(resourcePath, (error, data) => {
    if (!error) {
      const responseData = JSON.parse(data.toString('utf8'))
      //TODO set specific headers
      response.end(JSON.stringify(responseData.body))
      return
    }

    fs.readFile(`test/resources/empty.json`, (error, data) => {
      response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
      response.end(data.toString('utf8'))
    })
  })
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
})

function urlToFile(url) {
  return url
    .replace(/\//g, '-')
    .split('?')
}
